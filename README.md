# ipmi-sel-archiver

This script uses pieces from:
1. https://github.com/angelos-se/hpcworks/blob/master/ipmi-sel-archiver.sh  
2. ~~https://github.com/angelos-se/Self-updating-shell-script-example~~  
(The updater logic in this version got a complete rewrite)

## Install
This following listing verify the code's self updating capability, switching from master branch to stable
```
tail -f /var/log/messages | grep ipmi-sel-arc &
curl -o /etc/cron.daily/ipmi-sel-archiver.sh https://gitlab.com/ct-hkhpc-public/ipmi-sel-archiver/raw/master/ipmi-sel-archiver.sh
chmod +x /etc/cron.daily/ipmi-sel-archiver.sh
/etc/cron.daily/ipmi-sel-archiver.sh
```