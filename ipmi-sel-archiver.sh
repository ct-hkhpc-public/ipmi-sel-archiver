#!/bin/bash
set -e

# This script shall be placed in /etc/cron.daily and made executable
# This script fetches System Event Log using ipmitool, sends new SEL to syslog
# and make a copy to local file system
ThisScriptVer="20.02.10.1946"
ThisScript="`readlink -f $0`"
UpdateLogName="ipmi-sel-archive"

# Load config
[ -s /etc/sysconfig/$(basename $ThisScript).conf ] && . /etc/sysconfig/$(basename $ThisScript).conf

# Defaults: Auto Update
UpdateState=${UpdateState:-"curl"}
UpdateCurlCmd=${UpdateCurlCmd:-"curl -s -o "}
UpdateCheck=${UpdateCheck:-"openssl-sha1"}
UpdateURI=${UpdateURI:-"https://gitlab.com/ct-hkhpc-public/ipmi-sel-archiver/raw/stable/ipmi-sel-archiver.sh"}
UpdateLogName=${UpdateLogName:-"self-updating-script"}
# Defaults: ipmi-sel-archiver
LOGPATH=${LOGPATH:-"/var/log/ipmi-sel-archive"}
SELDUMP=${SELDUMP:-"/var/run/ipmi-sel-archive.log"}
SELHIGHWATERMARK=${SELHIGHWATERMARK:-"80"}

Update_and_Run () {
    # Configured to perform update
    if [ "$UpdateState" = "curl" ]; then
        UpdateSource=`mktemp`; UpdateSHA1=$UpdateSource.sha1
        # Test if we have working openssl sha1 checksum engine
        if echo "openssl test" | openssl sha1 |& grep -q 54a13776; then
            # openssl working as expected
            UpdateCheck="openssl-sha1-working"
        else
            # No we don't, not even trying
            export UpdateCheck="openssl-sha1-error"
            rm $UpdateSource; false; return
        fi
        # Fetch sha1 digest and determine if we should use curl or wget
        if ! $UpdateCurlCmd $UpdateSHA1 $UpdateURI.sha1; then
            wget -q -O $UpdateSHA1 $UpdateURI.sha1 && UpdateCurlCmd="wget -q -O "
        fi
        # Proceed with tests if we did not just fetch an empty file
        if [ -s $UpdateSHA1 ]; then
            # See if we already have latest
            if openssl sha1 $ThisScript | grep -q "$(head -1 $UpdateSHA1)"; then
                # Yes, we are good
                export UpdateState="nodiff"
                rm $UpdateSource $UpdateSHA1; false; return
            else
                # No, we need to update
                if $UpdateCurlCmd $UpdateSource $UpdateURI \
                  && openssl sha1 $UpdateSource \
                  |& grep -q "$(head -1 $UpdateSHA1)"; then
                    # Good checksum; update, clean-up and immediately execute new version
                    install -oroot -groot -m700 $UpdateSource $ThisScript \
                      && UpdateState="done" \
                      && rm $UpdateSource $UpdateSHA1 2>&1 | logger -t $UpdateLogName \
                      && echo "Running $(basename $ThisScript) version is $ThisScriptVer, update state is $UpdateState, digest state is $UpdateCheck" | logger -t $UpdateLogName \
                      && exec bash $ThisScript 
                    # Catch us here in case the long line above somehow failed
                    export UpdateState="patcherr"
                    rm $UpdateSource $UpdateSHA1; false; return
                else
                  # Bad checksum; abort updating
                  export UpdateState="badchecksum-or-curlerr"
                  export UpdateCheck="$(openssl sha1 $UpdateSource)"
                  rm $UpdateSource $UpdateSHA1; false; return
                fi
            fi
        else
            # Something wrong fetching sha1 digest
            export UpdateState="curlerr"
            rm $UpdateSource $UpdateSHA1; false; return
        fi
    else
        false; return
    fi
    # Assertion, how do we end up here?
    true; return
}

preflight () {
    BINS="ipmitool openssl readlink cut grep echo test hostname touch date"
    for i in $BINS; do which $i > /dev/null || exit 1; done

    # log path exists?
    if [ ! -d $LOGPATH ]; then
        mkdir -p $LOGPATH || exit 1
    fi

    # sensible SEL High Water Mark (SELHIGHWATERMARK)?
    [ $SELHIGHWATERMARK -gt 79 ] && [ $SELHIGHWATERMARK -lt 101 ] || exit 1

    # we will set BMC clock, we want a good system clock, do we have one?
    pgrep "(chronyd|ntpd)" > /dev/null || exit 1
  
    # is ipmi accessible?
    lsmod | grep -q ipmi_devintf || modprobe ipmi_devintf
    ipmitool chassis status &> /dev/null || exit 2
    # we are going to set bmc clock, log current bmc time just in case
    echo "bmc clock pre-sync: $(ipmitool sel time get)" | logger -t $UpdateLogName
    # setting clock as suggested by ipmitool man page (mostly concerning clearing sel)
    ipmitool sel time set "`date +"%m/%d/%Y %H:%M:%S"`" 2>&1 | logger -t $UpdateLogName
    echo "bmc clock post-sync: $(ipmitool sel time get)" | logger -t $UpdateLogName
}

host_id () {
    ipmitool sel elist > $SELDUMP || exit 2

    MACADDR="$(ipmitool lan print | grep -i 'mac addr' | grep -o -e '\(\S\S:\)\{5\}\S\S')"
    [ "$MACADDR" ] && export MACADDR || exit 3

    HOSTFQDN="$(hostname --fqdn)"
    [ "$HOSTFQDN" ] && export HOSTFQDN || exit 3

    SEL1="$(head -1 $SELDUMP)"
    [ "$SEL1" ] && export SELID="$(echo $MACADDR$SEL1 | openssl sha512 | cut -d' ' -f2 | cut -b1-6)" || exit 3
    export SELBASENAME="$HOSTFQDN-$MACADDR-$SELID.log"
    export SELOUT="$(readlink -m $LOGPATH/$SELBASENAME)"

    # make sure we can write out
    touch $SELOUT && [ -w "$SELOUT" ] || exit 1
}

clear_sel_when_full () {
    SELFull="$(ipmitool sel | grep Percent | cut -d: -f2 | cut -d% -f1)"
    [ $SELFull -gt $SELHIGHWATERMARK ] && ipmitool sel clear || true 
}

# Pull SEL from BMC
pull_sel_from_bmc () {
    SELDIFF=$SELDUMP.diff
    if diff -au $SELOUT $SELDUMP > $SELDIFF; then
        # diff empty, we supposed have archived everything
        # Let's empty SEL if it's nearly full.
        clear_sel_when_full
    else
        # we got something new
        export SELDIFF
    fi
}

# Push SEL to syslog
push_sel_to_syslog () {
    # make sure we don't hit the default rate limit of 200 messages / 5s; or we lose records
    if [ -s "$SELDIFF" ]; then
        curp=1 ; curq=20
        while [ "$(sed -n $curp,$curq\p <(grep "^+ " $SELDIFF | cut -d+ -f2-))" ]; do
            sed -n $curp,$curq\p <(grep "^+ " $SELDIFF | cut -d+ -f2-) | logger -t $UpdateLogName
            sleep 1
            curp="$(expr $curp + 20)"; curq="$(expr $curq + 20)"
        done
    # archive SEL to file system
    cp -f $SELDUMP $SELOUT
    fi
}

# Update routine runs before main program
if Update_and_Run; then
    echo "Updater assertion error, we shall never reach this place." | logger -t $UpdateLogName
    exit 1
else
    echo "Running $(basename $ThisScript) version is $ThisScriptVer, update state is $UpdateState, digest state is $UpdateCheck" | logger -t $UpdateLogName
fi

# Main program
preflight
host_id

pull_sel_from_bmc
push_sel_to_syslog

# Some old version uses this magic word
# M@GIC
